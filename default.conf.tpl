server {
    listen ${LISTEN_PORT};

    location /static {
        alias /vol/static;
    }

    location /admin {
        uwsgi_pass              ${APP_HOST}:${APP_PORT};
        include                 /etc/nginx/uwsgi_params;
        client_max_body_size    10M;
    }

    location /api {
            uwsgi_pass              ${APP_HOST}:${APP_PORT};
            include                 /etc/nginx/uwsgi_params;
            client_max_body_size    10M;
    }

    location / {
       root /usr/share/nginx/html;
     }
}